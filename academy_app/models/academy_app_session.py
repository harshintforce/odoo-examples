from odoo import api, fields, models
from odoo.exceptions import ValidationError

class Session(models.Model):
    _name = 'academy.session'
    _description = 'Session Module'

    name = fields.Char(string="Session Name" , required=True)
    start_date = fields.Date("Start Date", default=fields.Date.today)
    duration = fields.Float()
    description = fields.Text()
    course_description = fields.Text()
    seats = fields.Integer("Number of Seats")
    active = fields.Boolean(default=True)
    state = fields.Selection([('draft', 'Draft'), ('confirmed', 'Confirmed'), ('done', 'Done')], 
    required=True, default='draft')
    instructor_id = fields.Many2one('res.partner')
    course_id = fields.Many2one('academy.course')
    attendee_ids = fields.Many2many('res.partner')
    course_description = fields.Text(related="course_id.description")
    date = fields.Date(string="Session Date")
    attendee_count = fields.Integer(compute='calculate_occupation', store=True)
    occupation = fields.Float(compute='calculate_occupation')

    @api.model
    def create(self, values):  # override
        # other code
        return super(Session, self).create(values)

    @api.multi
    def write(self, values):
        # other code
        return super(Session, self).write(values)

    @api.multi
    def copy(self, default=None):
        default = dict(default or {})
        if 'name' not in default:
            default['name'] = ("%s (copy)") % (self.name)
        return super(Session, self).copy(default=default)

    @api.multi
    def unlink(self):
        if self.name == "test":
            raise ValidationError(
                "Recrod name having test not deletable")
        return super(Session, self).unlink()

    def do_confirm(self):
        self.state = 'confirmed'

    def do_done(self):
        self.state = 'done'

    # constrains are only checked at the time of save
    @api.constrains('attendee', 'instructor')
    def attendee_const(self):
        if self.instructor.id in self.attendee.ids:
            raise ValidationError('You can not add instructor in attendee')

    @api.depends('attendee_ids', 'seats')
    def calculate_occupation(self):
        for session in self:
            session.attendee_count = len(session.attendee_ids)
            if session.seats:
                session.occupation = len(session.attendee_ids) * 100 / session.seats
            else:
                session.occupations = 0.0