from odoo import api, fields, models


class Course(models.Model):
    _name = 'academy.course'
    _description = 'Courses , Session , Attendees , Instructor'

    name = fields.Char(string='Title', help="this is test", required=True)
    description = fields.Text(string='Description')
    session_ids = fields.One2many('academy.session', 'course_id')
    html = fields.Html()
    upper = fields.Char(compute='_compute_upper', inverse='_inverse_upper', readonly=False)
    responsible = fields.Many2one('res.users')
    image = fields.Binary()


    @api.depends('name')
    def _compute_upper(self):
        for rec in self:
            rec.upper = rec.name.upper() if rec.name else False
            
    @api.depends('upper')
    def _inverse_upper(self):
        for rec in self:
            rec.name = rec.upper.lower() if rec.upper else False
