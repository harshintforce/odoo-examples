{
    'name': 'Academy Course',
    'version': '1.0',
    'summary': 'Courses , Session , Attendees , Instructor',
    'category': 'Tools',
    'author': 'Harsh Shah',
    'maintainer': 'Harsh Shah',
    'website': 'https://intforce.co.in',
    'license': '',
    'contributors': [
        '',
    ],
    'depends': [
        'base',
    ],
    'data': [
        'views/academy_course_views.xml',
        'views/academy_session_views.xml',
        'security/ir.model.access.csv',
    ],
    'installable': True,
    'auto_install': True,
    'application': True,
}